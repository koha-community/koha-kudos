#!/usr/bin/perl

use Git::Wrapper;
use Modern::Perl;
use JSON;
use Getopt::Long;
use Pod::Usage;

my $gitpath;
my $tag;
my $help;
my $orgmap;

GetOptions(
    'path=s'   => \$gitpath,
    'tag=s'    => \$tag,
    'h|help'   => \$help,
    'orgmap=s' => \$orgmap,

);
unless ( $gitpath && $tag && $orgmap ) {
    $help = 1;
}

if ($help) {
    pod2usage(
        -verbose => 1,
        -exitval => 0
    );
}
open( my $orgfile, "<", $orgmap ) || die "Can't open file $orgmap";

my $git = Git::Wrapper->new($gitpath);
$git->fetch;
$git->checkout("origin/main");

my %authors;

my %orgs;
while ( my $line = <$orgfile> ) {
    chomp $line;
    my @row = split( /,/, $line );
    $orgs{ $row[0] } = $row[1];
}
close $orgfile;

my @log_objects = $git->log("$tag..HEAD");
foreach my $log (@log_objects) {

    if ( !$authors{ $log->author } ) {
        my $authhash = { 'count' => 1, 'signoffs' => 0 };
        $authors{ $log->author } = $authhash;
    }
    else {
        $authors{ $log->author }->{'count'}++;
    }
    my $message = $log->message;
    while ( $message =~ /Signed-off-by: (.*?)\n/g ) {
        if ( !$authors{$1} ) {
        }
        else {
            $authors{$1}->{'signoffs'}++;
        }

    }
}

my %orgcounts;

foreach my $author ( keys %authors ) {
    my $match = 0;
    foreach my $org ( keys %orgs ) {
        if ( $author =~ /$org/ ) {
            if ( !$orgcounts{ $orgs{$org} } ) {
                my $orghash = {
                    'count'    => $authors{$author}->{'count'},
                    'signoffs' => $authors{$author}->{'signoffs'}
                };

                $orgcounts{ $orgs{$org} } = $orghash;
            }
            else {
                $orgcounts{ $orgs{$org} }->{'count'} +=
                  $authors{$author}->{'count'};
                $orgcounts{ $orgs{$org} }->{'signoffs'} +=
                  $authors{$author}->{'signoffs'};
            }
            $match = 1;
            last;
        }
    }
    unless ($match) {
        if ( !$orgcounts{$author} ) {
            my $orghash = {
                'count'    => $authors{$author}->{'count'},
                'signoffs' => $authors{$author}->{'signoffs'}
            };
            $orgcounts{$author} = $orghash;
        }
        else {
            $orgcounts{$author}->{'count'} +=
              $authors{$author}->{'count'};
            $orgcounts{$author}->{'signoffs'} +=
              $authors{$author}->{'signoffs'};

        }

    }

}
my $json = JSON->new->allow_nonref;
$json = $json->canonical(1);

my $json_text = $json->encode( \%orgcounts );
print $json_text;
